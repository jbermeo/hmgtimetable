<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role;
        $role->name = 'Supervisor';
        $role->active = '1';
        $role->save();

        $role = new Role;
        $role->name = 'Employee';
        $role->active = '1';
        $role->save();
    }
}
