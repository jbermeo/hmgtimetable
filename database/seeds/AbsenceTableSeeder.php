<?php

use Illuminate\Database\Seeder;
use App\Absence;

class AbsenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $absence = new Absence;
        $absence->name = 'Compensatory';
        $absence->active = '1';
        $absence->save();

        $absence = new Absence;
        $absence->name = 'Illness';
        $absence->active = '1';
        $absence->save();

        $absence = new Absence;
        $absence->name = 'Vacation';
        $absence->active = '1';
        $absence->save();

    }
}
