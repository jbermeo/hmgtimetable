<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AbsenceTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
