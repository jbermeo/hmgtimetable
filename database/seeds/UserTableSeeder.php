<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Employee;
use App\Role;
use App\Project;
use App\Setting;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->email = 'john@me.com';
        $user->password = bcrypt('1234567');
        $user->save();

        $employee = new Employee;
        $employee->user()->associate($user);
        $employee->code = 'COD1';
        $employee->first_name = 'John';
        $employee->last_name = 'Bermeo';
        $employee->position = 'Junior Consultan';
        $employee->active = '1';
        $employee->save();
        //attaching roles to the employee
        $roles = Role::where('active', 1)->get();
        $employee->roles()->attach($roles);
        // attaching projects to the employee
        $projects = Project::where('active', 1)->get();
        $employee->projects()->attach($projects);

        // asigning Settings to the Employee
        $setting = new Setting;
        $setting->year = '2020';
        $setting->vacations_days = '30';
        $setting->vacations_adjusting_days = '0';
        $setting->weekly_hours = json_encode('{"mon":8,"tue":8,"wed":8, "thu":8, "fri":8, "sat":0}');
        $setting->nightly_hours = json_encode('{"start":23,"stop":06}');
        $setting->maximum_hours = '10';
        $setting->active = 1;
        $settings = $employee->settings()->save($setting);
    }
}
