<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project = new Project;
        $project->code = 'PR1';
        $project->name = 'Project 1';
        $project->active = '1';
        $project->save();

        $project = new Project;
        $project->code = 'PR2';
        $project->name = 'Project 2';
        $project->active = '1';
        $project->save();

        $project = new Project;
        $project->code = 'PR3';
        $project->name = 'Project 3';
        $project->active = '1';
        $project->save();

        $project = new Project;
        $project->code = 'PR4';
        $project->name = 'Project 4';
        $project->active = '0';
        $project->save();

        $project = new Project;
        $project->code = 'PR5';
        $project->name = 'Project 5';
        $project->active = '0';
        $project->save();
    }
}
