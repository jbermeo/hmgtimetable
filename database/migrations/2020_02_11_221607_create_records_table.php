<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->foreign('employee_id')
                    ->references('id')->on('employees')
                    ->onDelete('cascade');

            $table->unsignedBigInteger('project_id')->nullable();
            $table->foreign('project_id')
                    ->references('id')->on('projects')
                    ->onDelete('cascade');

            $table->unsignedBigInteger('absence_id')->nullable();
            $table->foreign('absence_id')
                    ->references('id')->on('absences')
                    ->onDelete('cascade');

            $table->date('date');
            $table->time('from_time');
            $table->time('until_time');
            $table->time('break_duration');
            $table->text('task_description');
            $table->float('working_time', 3, 1);
            $table->float('diference_time', 3, 1);
            $table->float('night_working_time', 3, 1);
            $table->float('expected_time', 3, 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
