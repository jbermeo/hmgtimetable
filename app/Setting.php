<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * Get the employee that owns the setting.
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
