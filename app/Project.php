<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The employees that belong to the role.
     */
    public function employees()
    {
        return $this->belongsToMany(Employee::class);
    }

    /**
     * Get the records for the project.
     */
    public function records()
    {
        return $this->hasMany(Record::class);
    }
}
