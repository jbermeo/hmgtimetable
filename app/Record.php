<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    /**
     * Get the employee that owns the record.
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * Get the project that owns the record.
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the absence that owns the record.
     */
    public function absence()
    {
        return $this->belongsTo(Absence::class);
    }
}
