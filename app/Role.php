<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The employees that belong to the role.
     */
    public function employees()
    {
        return $this->belongsToMany(Employee::class);
    }
}
