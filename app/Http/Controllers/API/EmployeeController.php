<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ResponseController as ResponseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Employee;
use App\Role;
use App\Project;
use App\Setting;
use Validator;


class EmployeeController extends ResponseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //$r = json_decode($input['roles'], true);
        $user = new User;
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->save();
        if($user){
            $employee = new Employee;
            $employee->user()->associate($user);
            $employee->code = $input['code'];
            $employee->first_name = $input['first_name'];
            $employee->last_name = $input['last_name'];
            $employee->position = $input['position'];
            $employee->active = $input['active'];
            $employee->save();
            //attaching roles to the employee
            $roles_array = array();
            foreach (json_decode($input['roles'], true) as $rol ) {
                array_push($roles_array, $rol);
            }
            $roles = Role::find($roles_array)->where('active', 1);;
            $employee->roles()->attach($roles);

            // attaching projects to the employee
            $projects_array = array();
            foreach (json_decode($input['projects'], true) as $project ) {
                array_push($projects_array, $project);
            }
            $projects = Project::find($projects_array)->where('active', 1);
            $employee->projects()->attach($projects);

            // asigning Settings to the Employee
            $setting = new Setting;
            $setting->year = $input['year'];
            $setting->vacations_days = $input['vacations_days'];
            $setting->vacations_adjusting_days = $input['vacations_adjusting_days'];
            $setting->weekly_hours = json_encode($input['weekly_hours']);
            $setting->nightly_hours = json_encode($input['nightly_hours']);
            $setting->maximum_hours = $input['maximum_hours'];
            $setting->active = 1;
            $settings = $employee->settings()->save($setting);

            $employee['roles'] = $roles;
            $employee['projects'] = $projects;
            $employee['settings'] = $settings;

            $success['employee'] = $employee;
            return $this->sendResponse($success);
        }
        else{
            $error = "Sorry! Registration is not successfull.";
            return $this->sendError($error, 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
