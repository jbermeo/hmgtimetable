<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * Get the user that owns the employee.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The roles that belong to the employee.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * The projects that belong to the employee.
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

    /**
     * Get the settings for the employee.
     */
    public function settings()
    {
        return $this->hasMany(Setting::class);
    }

    /**
     * Get the records for the employee.
     */
    public function records()
    {
        return $this->hasMany(Record::class);
    }
}
