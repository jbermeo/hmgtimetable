<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    /**
     * Get the records for the absence.
     */
    public function records()
    {
        return $this->hasMany(Record::class);
    }
}
